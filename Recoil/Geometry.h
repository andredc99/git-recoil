#pragma once
#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>

class Geometry
{
public:
	Geometry();
	~Geometry();

	b2Body* ground(b2World* world_, sf::Vector2f position_, sf::Vector2f size_);
	b2Body* object(b2World* world_, sf::Vector2f position_, sf::Vector2f size_, bool collide_);
	sf::RectangleShape sprite(sf::Vector2f position_, sf::Vector2f size_);

	const float scale = 30.0f;
};

