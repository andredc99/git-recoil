#include "GamePauseOverlay.h"



GamePauseOverlay::GamePauseOverlay()
{
}

bool GamePauseOverlay::init(Resources* data_, sf::RenderWindow* window_)
{
	data = data_;
	window = window_;
	window->setFramerateLimit(60);

	//	Define the background that will show
	bg.setFillColor(data->main_yellow);
	bg_x = -600;
	bg.setPosition(bg_x, 0);
	sf::Vector2f bg_size(600, window->getSize().y);
	bg.setSize(bg_size);

	//	Define the selections text
	sf::Vector2f text_position(80.0f, 250.0f);
	for (int i = 0; i < 2; i++) {
		selections[i].setFont(data->default_font);
		selections[i].setCharacterSize(40);
		selections[i].setPosition(text_position);
#pragma warning(suppress : 4996) 
		selections[i].setColor(data->main_blue);	
		text_position += sf::Vector2f(0.0f, 60.0f);
	}
	selections[0].setString("Resume");
	selections[1].setString("Main Menu");

	//	Highlight specifications
	sf::Vector2f highlight_pos(70.0f, 250.0f);
	for (int i = 0; i < 2; i++) {
		highlights[i].setFillColor(data->main_blue);
		highlights[i].setPosition(highlight_pos);
		highlights[i].setSize(sf::Vector2f(250.0f, 50.0f));
		highlight_pos += sf::Vector2f(0.0f, 60.0f);
	}

	//	Title text specs
	title_text.setFont(data->default_font);
	title_text.setCharacterSize(65);
	title_text.setPosition(sf::Vector2f(80.0f, 125.0f));
#pragma warning(suppress : 4996) 
	title_text.setColor(data->main_blue);
	title_text.setString("PAUSED");

	allow_esc = false;
	opening = true;
	update();

	return true;
}

void GamePauseOverlay::handleInput(float dt)
{
	resetKeys();
	//	If you press down and are allowed to go down:
	if (is[1] && !was[1] && selection_index < 1) {
		selection_index++;
		update();
	}
	//	If you press up and are allowed to go up:
	if (is[2] && !was[2] && selection_index > 0) {
		selection_index--;
		update();
	}
	if (is[3] && !was[3]) {
		activate(selection_index);
	}
	if (is[4] && !was[4]) {
		activate(0);
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
		activate(selection_index);
	}


	//	Mouse input
	for (int i = 0; i < 2; i++) {
		if (sf::Mouse::getPosition().x < (highlights[i].getPosition().x + highlights[i].getSize().x) && sf::Mouse::getPosition().x >(highlights[i].getPosition().x)) {
			if (sf::Mouse::getPosition().y > (highlights[i].getPosition().y) && sf::Mouse::getPosition().y < (highlights[i].getPosition().y + highlights[i].getSize().y)) {
				selection_index = i;
				update();
			}
		}
	}

	//	Lerp background upon entry
	if (opening) {
		leaving = false;
		bg_x += (0 - bg_x) * 0.2;
		bg.setPosition(bg_x, 0);

		if (bg_x > -1) {
			bg_x = 0;
			opening = false;
			allow_esc = true;
		}
	}

	//	Lerp background upon exit
	if (leaving && allow_esc) {
		float target = -600;

		bg_x += (target - bg_x) * 0.45;
		bg.setPosition(bg_x, 0);

		if (bg_x < -599) {
			bg_x = -600;
			data->active_focus = data->last_focus;			
			opening = true;
			allow_esc = false;
		}
	}
}

void GamePauseOverlay::resetKeys()
{
	for (int i = 0; i < 4; i++) {
		was[i] = is[i];
	}
	is[0] = sf::Mouse::isButtonPressed(sf::Mouse::Left);
	is[1] = data->input.isKeyDown(sf::Keyboard::Down);
	is[2] = data->input.isKeyDown(sf::Keyboard::Up);
	is[3] = data->input.isKeyDown(sf::Keyboard::Enter);	
	is[4] = data->input.isKeyDown(sf::Keyboard::Escape);
}

void GamePauseOverlay::update()
{
	for (int i = 0; i < 2; i++) {
#pragma warning(suppress : 4996) 
		selections[i].setColor(data->main_blue);
	}
#pragma warning(suppress : 4996) 
	selections[selection_index].setColor(data->main_yellow);
}

void GamePauseOverlay::activate(int index_)
{
	switch (index_) {
	case 0:
		leaving = true;
		break;
	case 1:
		data->active_focus = data->enum_menu;
		break;
	default:
		break;
	}
}

void GamePauseOverlay::render()
{
	//window->clear(sf::Color::Black);

	window->draw(bg);
	//	Draw the highlight depending on the player selection
	if (!leaving) {
		window->draw(highlights[selection_index]);
		window->draw(title_text);
	}

	//	Draw the menu text
	if (!leaving) {
		for (int i = 0; i < 2; i++) {
			window->draw(selections[i]);
		}
	}

	window->display();
}

void GamePauseOverlay::clean()
{
	data = NULL;
	delete data;

	window = NULL;
	delete window;
}