#pragma once
#include <iostream>
#include "Input.h"
#include "Geometry.h"
#include "XmlWriter.h"
#include "LevelData.h"
#include <thread>

class Resources
{
public:
	Resources();

	const int scale = 30;

	sf::RenderWindow* window;
	sf::Cursor cursor_;
	Input input;
	LevelData level_manager;
	Geometry geo_mgr;
	b2World* world;

	//	Player getter and setter
	inline void setPlayer(bool b) { player_num = b; }
	inline bool getPlayer() { return player_num; }

	//	Aesthetic variables
	sf::Font default_font;
	sf::Font game_font;
	sf::Color main_yellow;
	sf::Color main_blue;
	sf::Color ui_full;
	sf::Color ui_empty;

	//	Game assets
	sf::Texture ground_tex;
	sf::Texture bg_cloud;
	sf::Texture bg_tree;

	sf::Texture icon_reload;

	//	Resolution
	sf::Vector2f getResolution() { return current_resolution; }

	//	Hardware
	int threadCount() { return cpu_thread_count; }

	//	Active game enum
	enum FOCUS {
		enum_menu,
		enum_picker,
		enum_level,
		enum_pause,
		enum_edit,
		enum_options
	};

	FOCUS active_focus;
	FOCUS last_focus;

	//	game load bools
	bool loaded[5];	//	0 = menu; 1 = game; 2 = worldbuilder; 3 = pause menu; 4 = level picker;



private:
	sf::Vector2f resolution[4];
	sf::Vector2f current_resolution;

	int cpu_thread_count;

	//	Player bool
	//	0 = player 1; 1 = player 2
	bool player_num;

};

