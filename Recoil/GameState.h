#pragma once

//enum direction : char { left = 'l', right = 'r' };

enum class State { MENU, LEVEL, PAUSE};

class GameState
{
public:
	//GameState();

	void setCurrentState(State s);
	State getCurrentState();

	void setLastState(State s);
	State getLastState();

	enum GState { MENU, LEVEL, PAUSE};

protected:
	State currentState, lastState;
};