#include "Geometry.h"



Geometry::Geometry()
{
}


Geometry::~Geometry()
{
}

b2Body* Geometry::ground(b2World* world_, sf::Vector2f position_, sf::Vector2f size_)
{
	b2BodyDef BodyDef;
	BodyDef.position = b2Vec2(position_.x / scale, position_.y / scale);
	BodyDef.type = b2_staticBody;
	BodyDef.fixedRotation = true;
	b2Body* Body = world_->CreateBody(&BodyDef);

	b2PolygonShape Shape;
	Shape.SetAsBox((size_.x / 2) / scale, (size_.y / 2) / scale);
	b2FixtureDef FixtureDef;
	FixtureDef.density = 0.f;
	FixtureDef.shape = &Shape;
	Body->CreateFixture(&FixtureDef);

	return Body;
}

b2Body* Geometry::object(b2World* world_, sf::Vector2f position_, sf::Vector2f size_, bool collide_)
{
	b2BodyDef BodyDef;
	BodyDef.position = b2Vec2(position_.x / scale, position_.y / scale);
	BodyDef.type = b2_dynamicBody;
	BodyDef.fixedRotation = true;
	b2Body* Body = world_->CreateBody(&BodyDef);

	b2PolygonShape Shape;
	Shape.SetAsBox((size_.x / 2) / scale, (size_.y / 2) / scale);
	b2FixtureDef FixtureDef;
	if (collide_) { FixtureDef.isSensor = true; }
	FixtureDef.density = 1.f;
	FixtureDef.friction = 0.7f;
	FixtureDef.shape = &Shape;
	Body->CreateFixture(&FixtureDef);

	return Body;
}

sf::RectangleShape Geometry::sprite(sf::Vector2f position_, sf::Vector2f size_)
{
	sf::RectangleShape sprite_;
	sprite_.setSize(size_);
	sprite_.setOrigin(size_.x / 2, size_.y / 2);
	sprite_.setFillColor(sf::Color::Yellow);	
	
	return sprite_;
}