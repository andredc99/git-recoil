#pragma once
#include "Resources.h"

class GamePauseOverlay
{
public:
	GamePauseOverlay();

	bool init(Resources* data_, sf::RenderWindow* window_);
	void handleInput(float dt);
	void render();
	void clean();

private:
	Resources* data;
	sf::RenderWindow* window;
	void update();
	void resetKeys();
	void activate(int i_);

	//	Key refreshers
	bool is[5];		// 0 = mouse left click; 1 = arrow down; 2 = arrow up; 3 = enter; 4 = esc;
	bool was[5];	// 0 = mouse left click; 1 = arrow down; 2 = arrow up; 3 = enter; 4 = esc;
	bool allow_esc;
	bool opening;
	bool leaving;

	//	UI elements of the menu
	sf::RectangleShape bg;
	sf::RectangleShape highlights[2];		//	0 = resume; 1 = main menu;
	sf::Text	title_text;
	sf::Text	selections[2];				//	0 = resume, 1 = main menu;
	int			selection_index;			//	0 = resume; 1 = main menu;

	//	Lerp bg variables
	float bg_x;
	bool first = true;
};

