#pragma once
#include "SpriteTemplate.h"
#include "Resources.h"

class WinCondition :
	public SpriteTemplate
{
public:
	WinCondition();
	~WinCondition();

	bool init(Resources* data_, b2Vec2 position_);

private:
	Resources* data;
};

