#include "WinCondition.h"



WinCondition::WinCondition()
{
	setType(WIN);
}


WinCondition::~WinCondition()
{
}

bool WinCondition::init(Resources* data_, b2Vec2 position_)
{
	data = data_;
	sf::Vector2f size_(data->getResolution().x / 32, data->getResolution().y / 18);
	sf::Vector2f start_point(size_.x / 2, size_.y / 2);
	sf::Vector2f new_coords(start_point.x + (position_.x * size_.x), start_point.y + (position_.y * size_.y));

	b2BodyDef BodyDef;
	BodyDef.position = b2Vec2(new_coords.x / data->scale, new_coords.y / data->scale);
	BodyDef.type = b2_staticBody;
	b2Body* Body = data->world->CreateBody(&BodyDef);

	b2PolygonShape Shape;
	Shape.SetAsBox((size_.x / 2) / data->scale, (size_.y / 2) / data->scale);
	b2FixtureDef FixtureDef;
	FixtureDef.density = 0.f;
	FixtureDef.shape = &Shape;
	FixtureDef.isSensor = true;
	Body->CreateFixture(&FixtureDef);

	Body->SetUserData(this);

	setSize(size_);
	setOrigin(size_.x / 2, size_.y / 2);
	setFillColor(sf::Color::Green);
	setPosition(Body->GetPosition().x * data->scale, Body->GetPosition().y * data->scale);

	return true;
}
