#include "Resources.h"
#include "GameState.h"

#include "LoadingScreen.h"

#include "MainMenu.h"
#include "LevelSelect.h"
#include "Playground.h"
#include "Level1.h"
#include "GamePauseOverlay.h"
#include "MapMaker.h"
#include "Forger.h"
#include "LevelData.h"

void main()
{
	//	Initialise window and create loading screen
	sf::RenderWindow window(sf::VideoMode(1920, 1080), "Recoil", sf::Style::Fullscreen);
	Resources data_mgr;
	LoadingScreen loading_screen_;
	loading_screen_.init(&data_mgr, &window);
	loading_screen_.render(0);

	//	Initialise default values
	sf::Clock clock;
	float deltaTime;						loading_screen_.render(20);
	
	//	Declare cursor
	if (data_mgr.cursor_.loadFromSystem(sf::Cursor::Arrow)) {
		window.setMouseCursor(data_mgr.cursor_);
		window.setMouseCursorVisible(true);
	}										loading_screen_.render(30);

	//	Create class objects
	MainMenu menu;							loading_screen_.render(40);
	LevelSelect level_select;				loading_screen_.render(45);
	Level1 level_1;							loading_screen_.render(50);
	Forger forge;							loading_screen_.render(60);
	GamePauseOverlay pause;					loading_screen_.render(70);
	LevelData level_mgr;
	
	//	Run class init functions
	menu.init(&data_mgr, &window);			loading_screen_.render(80);
	level_select.init(&data_mgr, &window);	loading_screen_.render(83);
	forge.init(&data_mgr, &window);			loading_screen_.render(85);
	pause.init(&data_mgr, &window);			loading_screen_.render(90);	

	data_mgr.active_focus = data_mgr.enum_menu;

	// Game loop
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::Resized:
				window.setView(sf::View(sf::FloatRect(0.f, 0.f,
					(float)event.size.width, (float)event.size.height)));
				break;
			case sf::Event::KeyPressed:
				// update input class
				data_mgr.input.setKeyDown(event.key.code);
				break;
			case sf::Event::KeyReleased:
				//update input class
				data_mgr.input.setKeyUp(event.key.code);
				break;
			case sf::Event::MouseMoved:
				//update input class
				data_mgr.input.setMousePosition(event.mouseMove.x,
					event.mouseMove.y);
				break;
			case sf::Event::MouseButtonPressed:
				if (event.mouseButton.button == sf::Mouse::Left)
				{
					//update input class
					data_mgr.input.setMouseLeftDown(true);
				}
				break;
			case sf::Event::MouseButtonReleased:
				if (event.mouseButton.button == sf::Mouse::Left)
				{
					//update input class
					data_mgr.input.setMouseLeftDown(false);
				}
				break;
			default:
				// don't handle other events
				break;
			}
		}
		deltaTime = clock.restart().asSeconds();
		
		switch (data_mgr.active_focus) {
		case data_mgr.enum_menu:
			if (!data_mgr.loaded[0]) {
				loading_screen_.render(0);
				menu.init(&data_mgr, &window);
				data_mgr.loaded[0] = true;

				level_1.clean();
				data_mgr.loaded[1] = false;
				forge.clean();
				data_mgr.loaded[2] = false;
				level_select.clean();
				data_mgr.loaded[4] = false;
				pause.clean();
				data_mgr.loaded[3] = false;
			}
			menu.handleInput(deltaTime);
			menu.render();
			break;
		case data_mgr.enum_picker:
			if (!data_mgr.loaded[4]) {
				loading_screen_.render(0);
				level_select.init(&data_mgr, &window);
				data_mgr.loaded[4] = true;

				menu.clean();
				data_mgr.loaded[0] = false;
				pause.clean();
				data_mgr.loaded[3] = false;
			}
			level_select.handleInput(deltaTime);
			level_select.render();
			break;
		case data_mgr.enum_level:
			if (!data_mgr.loaded[1]) {
				loading_screen_.render(0);
				level_1.init(&data_mgr, &window);
				data_mgr.loaded[1] = true;

				level_select.clean();
				data_mgr.loaded[4] = false;
				pause.clean();
				data_mgr.loaded[3] = false;
			}
			level_1.update(deltaTime, false, false);
			level_1.render();
			break;
		case data_mgr.enum_pause:
			if (!data_mgr.loaded[3]) {
				pause.init(&data_mgr, &window);
				data_mgr.loaded[3] = true;
			}
			pause.handleInput(deltaTime);
			if (data_mgr.loaded[1]) {
				level_1.render();
			}
			else if (data_mgr.loaded[2]) {
				forge.render();
			}			
			pause.render();
			break;
		case data_mgr.enum_edit:
			if (!data_mgr.loaded[2]) {
				loading_screen_.render(0);
				forge.init(&data_mgr, &window);
				data_mgr.loaded[2] = true;

				menu.clean();
				data_mgr.loaded[0] = false;
				pause.clean();
				data_mgr.loaded[3] = false;
			}
			forge.update(deltaTime, false, false);
			forge.render();
			break;
		}		
	}
}
