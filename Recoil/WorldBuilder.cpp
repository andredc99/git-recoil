#include "WorldBuilder.h"

GroundPiece::GroundPiece()
{
	setType(GEOMETRY);
}

RampPiece::RampPiece()
{
	setType(RAMP);
}

WorldBuilder::WorldBuilder()
{
}

bool WorldBuilder::init(Resources* data_)
{
	data = data_;

	return true;
}

//	World builder system works on a tile based system: 32 wide and 18 tall. Index beginning at 0
//	Uses the same coordinate system as SFML maths, top left corner is (0, 0)
//	Bottom right corner is (31, 17)

void WorldBuilder::create_platform(std::vector<b2Vec2> coords)
{
	for (int i = 0; i < coords.size(); i++) {

		sf::Vector2f size_(data->getResolution().x / 32, data->getResolution().y / 18);
		sf::Vector2f start_point(size_.x / 2, size_.y / 2);		
		sf::Vector2f new_coords(start_point.x + (coords[i].x * size_.x), start_point.y + (coords[i].y * size_.y));		

		b2BodyDef BodyDef;
		BodyDef.position = b2Vec2(new_coords.x / data->scale, new_coords.y / data->scale);
		BodyDef.type = b2_staticBody;
		b2Body* Body = data->world->CreateBody(&BodyDef);

		b2PolygonShape Shape;
		Shape.SetAsBox((size_.x / 2) / data->scale, (size_.y / 2) / data->scale);		
		b2FixtureDef FixtureDef;
		FixtureDef.density = 0.f;
		FixtureDef.shape = &Shape;
		Body->CreateFixture(&FixtureDef);

		Body->SetUserData(this);

		GroundPiece sprite_;
		sprite_.setSize(size_);
		sprite_.setOrigin(size_.x / 2, size_.y / 2);
		sprite_.setTexture(&data->ground_tex);
		sprite_.setPosition(Body->GetPosition().x * data->scale, Body->GetPosition().y * data->scale);

		ground_pieces[i].body	= Body;
		ground_pieces[i].sprite = sprite_;
		active_piece_index++;
	}
}

void WorldBuilder::create_border()
{
	//	Top border
	sf::Vector2f size_(data->getResolution().x, 1);
	sf::Vector2f new_coords(data->getResolution().x / 2, -1);

	b2BodyDef BodyDef;
	BodyDef.position = b2Vec2(new_coords.x / data->scale, new_coords.y / data->scale);
	BodyDef.type = b2_staticBody;
	b2Body* Body = data->world->CreateBody(&BodyDef);

	b2PolygonShape Shape;
	Shape.SetAsBox((size_.x / 2) / data->scale, (size_.y / 2) / data->scale);
	b2FixtureDef FixtureDef;
	FixtureDef.density = 0.f;
	FixtureDef.shape = &Shape;
	Body->CreateFixture(&FixtureDef);

	Body->SetUserData(this);

	border_pieces[0] = Body;
	
	//	Bottom border
	//	NO BOTTOM BORDER

	//	Right border
	sf::Vector2f size_3(1, data->getResolution().y);
	sf::Vector2f new_coords3(data->getResolution().x + 1, data->getResolution().y / 2);

	b2BodyDef BodyDef3;
	BodyDef3.position = b2Vec2(new_coords3.x / data->scale, new_coords3.y / data->scale);
	BodyDef3.type = b2_staticBody;
	b2Body* Body3 = data->world->CreateBody(&BodyDef3);

	b2PolygonShape Shape3;
	Shape3.SetAsBox((size_3.x / 2) / data->scale, (size_3.y / 2) / data->scale);
	b2FixtureDef FixtureDef3;
	FixtureDef3.density = 0.f;
	FixtureDef3.shape = &Shape3;
	Body3->CreateFixture(&FixtureDef3);

	Body3->SetUserData(this);

	border_pieces[2] = Body3;

	//	Left border
	sf::Vector2f size_4(1, data->getResolution().y);
	sf::Vector2f new_coords4(-1, data->getResolution().y / 2);

	b2BodyDef BodyDef4;
	BodyDef4.position = b2Vec2(new_coords4.x / data->scale, new_coords4.y / data->scale);
	BodyDef4.type = b2_staticBody;
	b2Body* Body4 = data->world->CreateBody(&BodyDef4);

	b2PolygonShape Shape4;
	Shape4.SetAsBox((size_4.x / 2) / data->scale, (size_4.y / 2) / data->scale);
	b2FixtureDef FixtureDef4;
	FixtureDef4.density = 0.f;
	FixtureDef4.shape = &Shape4;
	Body4->CreateFixture(&FixtureDef4);

	Body4->SetUserData(this);

	border_pieces[3] = Body4;
}

b2Body* WorldBuilder::BodyInformation(int i_)
{
	return ground_pieces[i_].body;
}

void WorldBuilder::cleanWorld()
{
	for (int i = 0; i < getActivePieceCount(); i++) {
		data->world->DestroyBody(ground_pieces[i].body);		
	}
}