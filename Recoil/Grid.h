#pragma once
#include "Resources.h"

enum classification {
	air,
	ground,
	enemy,
	spawn,
	spawn2	
};

class Grid
{
public:
	Grid();

	void update();

	void setHigh(bool b)				{ highlighted = b; }
	void setActive(bool b)				{ active = b; }
	void setElement(classification c)	{ classification_ = c; }
	classification getElement()			{ return classification_; }

	//	Coordinates of the grids upper left corner
	sf::Vector2f coords;
	sf::Vector2f grid_coords;

	//	Grid's specific border
	sf::RectangleShape	border;

	//	String of the enum elements UI
	std::string class_string;

private:
	//	What type of grid space is this
	classification		classification_;

	//	Is the grid being highlighted
	bool	active;
	bool	highlighted;
};

