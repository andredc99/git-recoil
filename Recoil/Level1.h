#pragma once
#include "LevelMaster.h"

#include "Player.h"
#include "WorldBuilder.h"
#include "WinCondition.h"
#include "UImanager.h"

#include <vector>

struct player_info {
	b2Vec2 body_position;
	int health;
	
	bool shot_fired;
	b2Vec2 impact_point;
};

class Level1 : public LevelMaster
{
public:
	Level1();
	bool init(Resources* data_, sf::RenderWindow* window_);
	void update(float dt, bool thread_input, bool thread_collsion);
	void handleInput(float dt);
	void collision();
	void render();
	void clean();

private:
	Resources* data;
	sf::RenderWindow* window;

	void initUI();
	void resetKeys();
	void cooldowns(float dt);
	void displayTracer();
	void shakeScreen(float dt);

	//	Prevent quick swapping
	bool mouse_released;

	//	UI
	UImanager UI;

	//	Create player
	Player player1;	
	Player player2;
	Player* active_player;
	Player* opponent;

	player_info player_info_;

	sf::Vertex tracer[2];
	bool display_tracer;
	float tracer_cooldown;
	float tracer_current;

	//	Point in the level that the player hitscan is intersecting with
	b2Vec2 intersection_p;

	//	Create world builder
	WorldBuilder geo;
	bool border_built = false;

	//	Camera
	sf::View player_view;

	//	Screen shake
	sf::RectangleShape shake_focus;
	sf::View shake_view;
	bool shaking;
	float shake_cooldown;
	float shake_time;

	//	Debug
	bool debugging;
	sf::Text debug_text;
	sf::Vertex player_ray[2];

	//	Button refreshers
	bool is[2];
	bool was[2];

	//	Game art
	sf::RectangleShape background_sprite;
	sf::RectangleShape tree_sprites;
	sf::RectangleShape vignette;


};

