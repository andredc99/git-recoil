#pragma once
#include "Resources.h"

class LevelMaster
{
public:
	LevelMaster();
	~LevelMaster();

	virtual bool init(Resources* data_, sf::RenderWindow* window_) = 0;
	virtual void update(float dt, bool thread_input, bool thread_collision) = 0;
	virtual void handleInput(float dt) = 0;
	virtual void collision() = 0;
	virtual void render() = 0;
	virtual void clean() = 0;
};

