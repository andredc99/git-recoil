#include "GameState.h"

void GameState::setCurrentState(State s)
{
	currentState = s;
}

State GameState::getCurrentState()
{
	return currentState;
}

void GameState::setLastState(State s)
{
	lastState = s;
}

State GameState::getLastState()
{
	return lastState;
}
