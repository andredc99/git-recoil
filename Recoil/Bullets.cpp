#include "Bullets.h"

Bullets::Bullets()
{
	setType(BULLET);
	bullet_active = false;
}


Bullets::~Bullets()
{
}

void Bullets::spawn(Resources* data_, b2Vec2 direction_, sf::Vector2f position_)
{
	sf::Vector2f size(8.0f, 8.0f);
	direction_ *= 100.0f;
	bullet_active = true;
	bullet_bodies = data_->geo_mgr.object(data_->world, position_, size, true);
	bullet_bodies->SetGravityScale(0.0f);
	bullet_bodies->ApplyLinearImpulseToCenter(direction_, true);
	bullet_bodies->SetUserData(this);

	setSize(size);
	setOrigin(size.x / 2, size.y / 2);
	setFillColor(sf::Color::Cyan);

	waiting_to_die = false;
}

void Bullets::update()
{
	setPosition(bullet_bodies->GetPosition().x * scale, bullet_bodies->GetPosition().y * scale);
	setRotation(bullet_bodies->GetAngle() * 180 / b2_pi);

	if (getPosition().x > 1920 || getPosition().x < 0) {
		bullet_active = false;
	}
	if (getPosition().y > 1080 || getPosition().y < 0) {
		bullet_active = false;
	}

	if (waiting_to_die) {
		bullet_bodies = NULL;
		bullet_active = false;
	}
}

void Bullets::kill()
{
	waiting_to_die = true;
}