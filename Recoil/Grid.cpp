#include "Grid.h"



Grid::Grid()
{	
	border.setSize(sf::Vector2f(40, 40));
	border.setOutlineThickness(2);
	border.setOutlineColor(sf::Color::Yellow);
	border.setFillColor(sf::Color::Transparent);

	classification_ = air;
	class_string = "Air";
}

void Grid::update()
{
	//	Declare colours for the element classifications
	sf::Color ground_c(139, 69, 19);
	sf::Color enemy_c(255, 0, 0);
	sf::Color spawn_c(0, 255, 0);
	sf::Color win_c(188, 143, 143);
	
	//	Update colours to let user know what selection has been made
	if (active) {
		border.setFillColor(sf::Color::Yellow);
	}
	else if (highlighted && !active) {
		border.setFillColor(sf::Color::White);
	}
	else {
		switch (classification_) {
		case air:			
			border.setFillColor(sf::Color::Transparent);
			class_string = "Air";
			break;
		case ground:
			border.setFillColor(ground_c);
			class_string = "Ground";
			break;
		case enemy:
			border.setFillColor(enemy_c);
			class_string = "Enemy";
			break;
		case spawn:
			border.setFillColor(spawn_c);
			class_string = "Spawn";
			break;
		case spawn2:
			border.setFillColor(win_c);
			class_string = "Win";
			break;
		}
	}
}