#include "LevelData.h"

LevelData::LevelData()
{
}


LevelData::~LevelData()
{
}

void LevelData::saveLevel(int a, int g, int e, int s, int w, int slot_)
{
	element_data_count[slot_][0] = a;
	element_data_count[slot_][1] = g;
	element_data_count[slot_][2] = e;
	element_data_count[slot_][3] = s;
	element_data_count[slot_][4] = w;
}

void LevelData::readLevel(int slot_)
{
	//	Reset vectors
	platform_coords.clear();
	enemy_coords.clear();
	spawn_coords = b2Vec2(0, 0);
	spawn2_coords = b2Vec2(0, 0);

	std::ifstream level_file_read;
	switch (slot_) {
	case 0:
		//	Load in slot 1
		level_file_read.open("custom_level1.csv");
		break;
	case 1:
		//	Load in slot 2
		level_file_read.open("custom_level2.csv");
		break;
	case 2:
		//	Load in slot 3
		level_file_read.open("custom_level3.csv");
		break;
	}	
	
	std::string value;
	int count = 0;
	b2Vec2 temp_coords(0, 0);	

	//	Get air count
	getline(level_file_read, value, ',');
	element_data_count[slot_][0] = std::stoi(value);

	//	Pass through all air spaces
	for (int i = 0; i < element_data_count[slot_][0]; i++) {
		getline(level_file_read, value, ',');
		getline(level_file_read, value, ',');
	}

	//	Get platform count
	getline(level_file_read, value, ',');
	element_data_count[slot_][1] = std::stoi(value);

	//	Place all platforms into vectors
	for (int i = 0; i < element_data_count[slot_][1]; i++) {
			getline(level_file_read, value, ',');
			temp_coords.x = std::stoi(value);
			getline(level_file_read, value, ',');
			temp_coords.y = std::stoi(value);	
			platform_coords.push_back(temp_coords);
	}

	//	Get enemy spawn count
	getline(level_file_read, value, ',');
	element_data_count[slot_][2] = std::stoi(value);

	//	Place all enemy spawn points into a vector
	for (int i = 0; i < element_data_count[slot_][2]; i++) {
		getline(level_file_read, value, ',');
		temp_coords.x = std::stoi(value);
		getline(level_file_read, value, ',');
		temp_coords.y = std::stoi(value);
		enemy_coords.push_back(temp_coords);
	}
	//	Get player spawn point
	getline(level_file_read, value, ',');
	temp_coords.x = std::stoi(value);
	getline(level_file_read, value, ',');
	temp_coords.y = std::stoi(value);
	spawn_coords = temp_coords;
	
	//	Finally get win square
	getline(level_file_read, value, ',');
	temp_coords.x = std::stoi(value);
	getline(level_file_read, value, ',');
	temp_coords.y = std::stoi(value);		
	spawn2_coords = temp_coords;	

	//	Done with file
	level_file_read.close();	
}

void LevelData::loadDefault()
{
	//	Reset vectors
	platform_coords.clear();
	enemy_coords.clear();
	spawn_coords = b2Vec2(0, 0);
	spawn2_coords = b2Vec2(0, 0);

	std::ifstream level_file_read;
	level_file_read.open("default_level.csv");

	std::string value;
	int count = 0;
	b2Vec2 temp_coords(0, 0);

	//	Get air count
	getline(level_file_read, value, ',');
	element_defualt_data[0] = std::stoi(value);

	//	Pass through all air spaces
	for (int i = 0; i < element_defualt_data[0]; i++) {
		getline(level_file_read, value, ',');
		getline(level_file_read, value, ',');
	}

	//	Get platform count
	getline(level_file_read, value, ',');
	element_defualt_data[1] = std::stoi(value);

	//	Place all platforms into vectors
	for (int i = 0; i < element_defualt_data[1]; i++) {
		getline(level_file_read, value, ',');
		temp_coords.x = std::stoi(value);
		getline(level_file_read, value, ',');
		temp_coords.y = std::stoi(value);
		platform_coords.push_back(temp_coords);
	}

	//	Get enemy spawn count
	getline(level_file_read, value, ',');
	element_defualt_data[2] = std::stoi(value);

	//	Place all enemy spawn points into a vector
	for (int i = 0; i < element_defualt_data[2]; i++) {
		getline(level_file_read, value, ',');
		temp_coords.x = std::stoi(value);
		getline(level_file_read, value, ',');
		temp_coords.y = std::stoi(value);
		enemy_coords.push_back(temp_coords);
	}
	//	Get player spawn point
	getline(level_file_read, value, ',');
	temp_coords.x = std::stoi(value);
	getline(level_file_read, value, ',');
	temp_coords.y = std::stoi(value);
	spawn_coords = temp_coords;

	//	Finally get win square
	getline(level_file_read, value, ',');
	temp_coords.x = std::stoi(value);
	getline(level_file_read, value, ',');
	temp_coords.y = std::stoi(value);
	spawn2_coords = temp_coords;

	//	Done with file
	level_file_read.close();
}