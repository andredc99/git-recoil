#include "LevelSelect.h"

LevelSelect::LevelSelect()
{
}

bool LevelSelect::init(Resources* data_, sf::RenderWindow* window_)
{
	data = data_;
	window = window_;
	window->setFramerateLimit(60);
	window->setMouseCursorVisible(true);
	data->active_focus = data->enum_picker;

	//	Initialise background
	background.setSize(data->getResolution());
	background.setPosition(0, 0);
	background.setFillColor(data->main_yellow);

	//	Create temp template specifications for the highlights
	sf::RectangleShape highlight_;
	highlight_.setFillColor(data->main_blue);
	highlight_.setSize(sf::Vector2f(540.0f, 50.0f));

	sf::Vector2f highlight_position(170.0f, 340.0f);

	//	Go through each highlight and give it the temp values
	for (int i = 0; i < 5; i++) {
		highlights[i] = highlight_;
		highlights[i].setPosition(highlight_position);
		targets[i] = highlight_position.y;
		highlight_position.y += 60.0f;
	}

	//	Highlight lerping
	highlight = highlights[0];
	highlight_y = targets[0];

	//	Create the template for the menu options
	sf::Text default_text;
	default_text.setFont(data->default_font);
	default_text.setCharacterSize(40);
	default_text.setOrigin(default_text.getCharacterSize() / 2, default_text.getCharacterSize() / 2);

	sf::Vector2f text_position(200.0f, 360.0f);

	for (int i = 0; i < 5; i++) {
		selections[i] = default_text;
		selections[i].setPosition(text_position);
		text_position.y += 60.0f;
	}
	selections[0].setString("DEFAULT LEVEL");
	selections[1].setString("CUSTOM LEVEL 1");
	selections[2].setString("CUSTOM LEVEL 2");
	selections[3].setString("CUSTOM LEVEL 3");
	selections[4].setString("BACK");

	//	Declare title font values
	title.setFont(data->default_font);
	title.setCharacterSize(80);
	title.setOrigin(default_text.getCharacterSize() / 2, default_text.getCharacterSize() / 2);
	title.setPosition(200.0f, 140.0f);
#pragma warning(suppress : 4996) 
	title.setColor(data->main_blue);
	title.setString("CHOOSE LEVEL");

	//	Reset press blockers
	mouse_released = false;
	enter_released = false;

	update();

	return true;
}

void LevelSelect::update()
{
	for (int i = 0; i < 5; i++) {
#pragma warning(suppress : 4996) 
		selections[i].setColor(data->main_blue);
	}
#pragma warning(suppress : 4996) 
	selections[selection_index].setColor(data->main_yellow);
}

void LevelSelect::handleInput(float dt)
{
	//	Make sure nothing is being pressed when new state begins
	if (!data->input.isMouseLeftDown()) {
		mouse_released = true;
	}
	if (!data->input.isKeyDown(sf::Keyboard::Enter)) {
		enter_released = true;
	}
	if (mouse_released && enter_released)
	{
		//	Update function
		resetKeys();

		if (is[2] && !was[2] && selection_index > 0) {
			selection_index--;
			update();
		}
		if (is[1] && !was[1] && selection_index < 4) {
			selection_index++;
			update();
		}
		if (is[3] && !was[3]) {
			activate(selection_index);
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
			activate(selection_index);
		}

		//	Mouse input
		for (int i = 0; i < 5; i++) {
			if (sf::Mouse::getPosition().x < (highlights[i].getPosition().x + highlights[i].getSize().x) && sf::Mouse::getPosition().x >(highlights[i].getPosition().x)) {
				if (sf::Mouse::getPosition().y > (highlights[i].getPosition().y) && sf::Mouse::getPosition().y < (highlights[i].getPosition().y + highlights[i].getSize().y)) {
					selection_index = i;
					update();
				}
			}
		}

		//	Highlight lerping motion moving
		highlight_y += (targets[selection_index] - highlight_y) * 0.5;
		highlight.setPosition(170.0f, highlight_y);
	}
}

void LevelSelect::resetKeys()
{
	for (int i = 0; i < 4; i++) {
		was[i] = is[i];
	}
	is[0] = sf::Mouse::isButtonPressed(sf::Mouse::Left);
	is[1] = data->input.isKeyDown(sf::Keyboard::Down);
	is[2] = data->input.isKeyDown(sf::Keyboard::Up);
	is[3] = data->input.isKeyDown(sf::Keyboard::Enter);
}

void LevelSelect::activate(int index_)
{
	if (index_ == 0) {		
		data->level_manager.setLoadedSlotNum(-1);
		data->active_focus = data->enum_level;
	}
	else if (index_ != 4) {
		data->level_manager.setLoadedSlotNum(index_-1);
		data->active_focus = data->enum_level;
	}
	else {
		data->active_focus = data->enum_menu;
	}
}

void LevelSelect::render()
{
	window->clear(sf::Color::Black);
	window->draw(background);

	//	Draw the highlight
	window->draw(highlight);

	//	Draw the menu text
	for (int i = 0; i < 5; i++) {
		window->draw(selections[i]);
	}	
	window->draw(title);

	window->display();
}

void LevelSelect::clean()
{
	data = NULL;
	delete data;
	
	window = NULL;
	delete window;	
}