#include "Playground.h"

Playground::Playground()
{
}

bool Playground::init(Resources* data_, sf::RenderWindow* window_)
{
	data = data_;
	window = window_;
	window->setFramerateLimit(60);
	window->setMouseCursorVisible(false);

	b2Vec2 gravity(0.0f, 20.0f);
	data->world = new b2World(gravity);

	world_builder.init(data);

	std::vector<sf::Vector2f> world_piece_coordinates;
	world_piece_coordinates.push_back(sf::Vector2f(600, 400));
	world_piece_coordinates.push_back(sf::Vector2f(760, 400));

	//world_builder.create_geometry(world_piece_coordinates);

	//player.init(data, true);

	sf::Vector2f screen(1920.0f, 1080.0f);
	player_view.setSize(screen);

	return true;
}

void Playground::update(float dt)
{
	data->world->Step(1 / 60.0f, 8, 3);

	player.update(dt);

	line[0] = sf::Vertex(sf::Vector2f(player.getPosition()));
	line[1] = sf::Vertex(sf::Vector2f(data->input.getMouseX(), data->input.getMouseY()));

	player_view.setCenter(player.getPosition());

	collision();
}

void Playground::collision()
{
	// get the head of the contact list
	b2Contact* contact = data->world->GetContactList();
	// get contact count
	int contact_count = data->world->GetContactCount();

	for (int contact_num = 0; contact_num < contact_count; ++contact_num)
	{
		if (contact->IsTouching())
		{
			// get the colliding bodies
			b2Body* bodyA = contact->GetFixtureA()->GetBody();
			b2Body* bodyB = contact->GetFixtureB()->GetBody();

			// DO COLLISION RESPONSE HERE
			Player* player_ptr = NULL;
			Bullets* bullet_ptr = NULL;

			SpriteTemplate* gameObjectA = NULL;
			SpriteTemplate* gameObjectB = NULL;

			gameObjectA = (SpriteTemplate*)bodyA->GetUserData();
			gameObjectB = (SpriteTemplate*)bodyB->GetUserData();

			if (gameObjectA)
			{
				if (gameObjectA->getType() == PLAYER)
				{
					player_ptr = (Player*)bodyA->GetUserData();
				}
				if (gameObjectA->getType() == BULLET)
				{
					bullet_ptr = (Bullets*)bodyA->GetUserData();
				}
			}

			if (gameObjectB)
			{
				if (gameObjectB->getType() == PLAYER)
				{
					player_ptr = (Player*)bodyB->GetUserData();
				}
				if (gameObjectB->getType() == BULLET)
				{
					bullet_ptr = (Bullets*)bodyB->GetUserData();
				}
			}

			if (bullet_ptr && !player_ptr)
			{
				bullet_ptr->kill();
			}
		}

		// Get next contact point
		contact = contact->GetNext();
	}
}

void Playground::handleInput(float dt)
{

}

void Playground::render()
{
	window->clear(sf::Color::Black);	

	//window->setView(player_view);
	window->draw(player);
	window->draw(player.getCursor());
	for (int i = 0; i < 2; i++) {
		if (player.gun_.bullet_[i].bullet_active) {
			window->draw(player.gun_.bullet_[i]);
		}
	}
	
	//for (int i = 0; i < 20; i++) {
	//	if (world_builder.active_piece[i]) {
	//		window->draw(world_builder.ground_pieces[i].sprite);
	//	}
	//}

	//window->draw(line, 2, sf::Lines);

	window->display();
}

void Playground::clean()
{
	delete data->world;
	data->world = NULL;
}