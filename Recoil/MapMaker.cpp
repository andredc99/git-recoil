#include "MapMaker.h"

MapMaker::MapMaker()
{
}

bool MapMaker::init(Resources* data_, sf::RenderWindow* window_)
{
	data = data_;
	window = window_;
	window->setFramerateLimit(60);
	window->setMouseCursorVisible(true);
	//	Let pause menu know you were in the editor
	data->last_focus = data->enum_edit;

	//	Border for the map view
	map_border.setSize(sf::Vector2f(1290, 730));
	map_border.setPosition(sf::Vector2f(60, 60));
	map_border.setFillColor(data->main_blue);

	//	Place all grid spaces correctly
	sf::Vector2f place(65, 65);
	for (int x = 0; x < 32; x++) {
		for (int y = 0; y < 18; y++) {
			grid_[x][y].border.setPosition(place);
			grid_[x][y].grid_coords = sf::Vector2f(x, y);
			place.y += 40;
		}
		place.y = 65;
		place.x += 40;
	}	

	//	Declare UI options information
	float text_pos_y = 180.0f;
	for (int i = 0; i < 6; i++) {
		options[i].setPosition(1600.0f, text_pos_y + 5.0f);
		options[i].setFont(data->default_font);
		options[i].setCharacterSize(25);
#pragma warning(suppress : 4996)
		options[i].setColor(data->main_blue);

		options_bg[i].setSize(sf::Vector2f(140.0f, 40.0f));
		options_bg[i].setPosition(1590.0f, text_pos_y);
		options_bg[i].setFillColor(data->main_yellow);

		text_pos_y += 50.0f;
	}
	//	Set strings for the options
	options[0].setString("Set air");
	options[1].setString("Set ground");
	options[2].setString("Set enemy");
	options[3].setString("Set spawn");
	options[4].setString("Set win block");
	options[5].setString("Save");

	//	Delcare basics for text information
	grid_info.setPosition(sf::Vector2f(1590.0f, 120.0f));
	grid_info.setFont(data->default_font);
	grid_info.setCharacterSize(40.0f);
#pragma warning(suppress : 4996)
	grid_info.setColor(data->main_blue);

	//	Don't allow save
	allow_save = false;
	is_spawn_placed = false;
	is_win_placed = false;

	//	Save hint declaration
	save_hint.setFont(data->default_font);
	save_hint.setCharacterSize(20);
#pragma warning(suppress : 4996)
	save_hint.setColor(data->main_yellow);
	save_hint.setString("You must place a spawn point \n and a win point to save this level!");
	show_hint = false;

	save_hint_bg.setSize(sf::Vector2f(250.0f, 50.0f));
	save_hint_bg.setFillColor(data->main_blue);

	//	Save slot menu declarations
	save_bg_accent.setSize(sf::Vector2f(900, 80));
	save_bg_accent.setPosition(510, 240);
	save_bg_accent.setFillColor(data->main_yellow);

	//	Blue background
	save_bg.setSize(sf::Vector2f(900, 250));
	save_bg.setPosition(510, 320);
	save_bg.setFillColor(data->main_blue);

	//	Dim the out of focus UI
	dim_bg.setSize(sf::Vector2f(1920, 1080));
	dim_bg.setPosition(0, 0);

	sf::Color grey_dark;
	grey_dark.r = 64;
	grey_dark.g = 64;
	grey_dark.b = 64;
	grey_dark.a = 170;
#pragma warning(suppress : 4996)
	dim_bg.setFillColor(grey_dark);
	
	//	Title info for this dialog box
	save_info.setPosition(sf::Vector2f(550, 260));
	save_info.setFont(data->default_font);
	save_info.setCharacterSize(35);
#pragma warning(suppress : 4996)
	save_info.setColor(data->main_blue);
	save_info.setString("Select a save slot!");

	//	Set all colours and sizes for the save slot text and backgrounds
	for (int i = 0; i < 4; i++) {		
		save_slot_names[i].setFont(data->default_font);
		save_slot_names[i].setCharacterSize(30);
#pragma warning(suppress : 4996)
		save_slot_names[i].setColor(data->main_yellow);		

		save_slot_bg[i].setSize(sf::Vector2f(330, 40));		
		save_slot_bg[i].setFillColor(data->main_yellow);
	}
	//	Dialog box menu options, just save slots and the back option
	save_slot_names[0].setPosition(sf::Vector2f(550, 350));
	save_slot_names[0].setString("Custom Save Slot [1]");
	save_slot_bg[0].setPosition(510, 350);

	save_slot_names[1].setPosition(sf::Vector2f(550, 400));
	save_slot_names[1].setString("Custom Save Slot [2]");
	save_slot_bg[1].setPosition(510, 400);

	save_slot_names[2].setPosition(sf::Vector2f(550, 450));
	save_slot_names[2].setString("Custom Save Slot [3]");	
	save_slot_bg[2].setPosition(510, 450);

	save_slot_names[3].setPosition(sf::Vector2f(550, 500));
	save_slot_names[3].setString("Back.");
	save_slot_bg[3].setPosition(510, 500);

	//	Save feedback UI
	save_success_msg.setFont(data->default_font);
	save_success_msg.setCharacterSize(40);
#pragma warning(suppress : 4996)
	save_success_msg.setColor(data->main_blue);
	save_success_msg.setPosition(550, 580);
	save_success_msg.setString("Saved Successfully!");

	save_success_bg.setSize(sf::Vector2f(900, 120));	
	save_success_bg.setFillColor(data->main_yellow);
	save_success_bg.setPosition(510, 570);

	//	Menu index for save selection dialog box
	save_index = -1;

	return true;
}

void MapMaker::update(float dt, bool thread_i, bool thread_c)
{
	sf::Color grey_dark;
	sf::Color grey_light;
	grey_dark.r = 128;
	grey_dark.g = 128;
	grey_dark.b = 128;

	grey_light.r = 169;
	grey_light.g = 169;
	grey_light.b = 169;
	if (!removed_focus) {

		if (data->input.isKeyDown(sf::Keyboard::LShift)) {
			area_mode = true;
		}
		if (!data->input.isKeyDown(sf::Keyboard::LShift)) {
			area_mode = false;
		}

		if (current_active) {
			grid_info.setString(std::to_string(locked_x) + ", " + std::to_string(locked_y) + " (" + current_active->class_string + ")");

			//	Add colour when a grid element is active
			for (int i = 0; i < 5; i++) {
#pragma warning(suppress : 4996)
				options_bg[i].setFillColor(data->main_yellow);
#pragma warning(suppress : 4996)
				options[i].setColor(data->main_blue);
			}

			//	Highlight the selections if allowed to click
			//	Selecting air
			if (sf::Mouse::getPosition().x > 1590.0f && sf::Mouse::getPosition().x < 1730.0f && sf::Mouse::getPosition().y > 180.0f && sf::Mouse::getPosition().y < 220.0f) {
#pragma warning(suppress : 4996)
				options_bg[0].setFillColor(data->main_blue);
#pragma warning(suppress : 4996)
				options[0].setColor(data->main_yellow);

				index = 0;
			}
			//	Selecting ground
			else if (sf::Mouse::getPosition().x > 1590.0f && sf::Mouse::getPosition().x < 1730.0f && sf::Mouse::getPosition().y > 230.0f && sf::Mouse::getPosition().y < 270.0f) {
#pragma warning(suppress : 4996)
				options_bg[1].setFillColor(data->main_blue);
#pragma warning(suppress : 4996)
				options[1].setColor(data->main_yellow);

				index = 1;
			}
			//	Selecting enemy
			else if (sf::Mouse::getPosition().x > 1590.0f && sf::Mouse::getPosition().x < 1730.0f && sf::Mouse::getPosition().y > 280.0f && sf::Mouse::getPosition().y < 320.0f) {
#pragma warning(suppress : 4996)
				options_bg[2].setFillColor(data->main_blue);
#pragma warning(suppress : 4996)
				options[2].setColor(data->main_yellow);

				index = 2;
			}
			//	Selecting spawn
			else if (sf::Mouse::getPosition().x > 1590.0f && sf::Mouse::getPosition().x < 1730.0f && sf::Mouse::getPosition().y > 330.0f && sf::Mouse::getPosition().y < 370.0f) {
#pragma warning(suppress : 4996)
				options_bg[3].setFillColor(data->main_blue);
#pragma warning(suppress : 4996)
				options[3].setColor(data->main_yellow);

				index = 3;
			}
			//	Selecting win
			else if (sf::Mouse::getPosition().x > 1590.0f && sf::Mouse::getPosition().x < 1730.0f && sf::Mouse::getPosition().y > 380.0f && sf::Mouse::getPosition().y < 420.0f) {
#pragma warning(suppress : 4996)
				options_bg[4].setFillColor(data->main_blue);
#pragma warning(suppress : 4996)
				options[4].setColor(data->main_yellow);

				index = 4;
			}
			else {
				index = -1;
			}
		}
		else {
			grid_info.setString("Nothing selected...");

			//	Grey out options if nothing is selected
			for (int i = 0; i < 5; i++) {
#pragma warning(suppress : 4996)
				options_bg[i].setFillColor(grey_light);
#pragma warning(suppress : 4996)
				options[i].setColor(grey_dark);
			}

			index = -1;
		}
		//	Save button information
		if (allow_save) {
#pragma warning(suppress : 4996)
			options_bg[5].setFillColor(data->main_yellow);
#pragma warning(suppress : 4996)
			options[5].setColor(data->main_blue);

			if (sf::Mouse::getPosition().x > 1590.0f && sf::Mouse::getPosition().x < 1730.0f && sf::Mouse::getPosition().y > 430.0f && sf::Mouse::getPosition().y < 470.0f) {
#pragma warning(suppress : 4996)
				options_bg[5].setFillColor(data->main_blue);
#pragma warning(suppress : 4996)
				options[5].setColor(data->main_yellow);

				index = 5;
			}
		}
		else {
#pragma warning(suppress : 4996)
			options_bg[5].setFillColor(grey_light);
#pragma warning(suppress : 4996)
			options[5].setColor(grey_dark);

			if (sf::Mouse::getPosition().x > 1590.0f && sf::Mouse::getPosition().x < 1730.0f && sf::Mouse::getPosition().y > 430.0f && sf::Mouse::getPosition().y < 470.0f) {
				show_hint = true;
				save_hint.setPosition(sf::Mouse::getPosition().x + 5.0f, sf::Mouse::getPosition().y + 15.0f);
				save_hint_bg.setPosition(sf::Mouse::getPosition().x + 2.0f, sf::Mouse::getPosition().y + 15.0f);
			}
			else {
				show_hint = false;
			}
		}
	}
	else {
		for (int i = 0; i < 4; i++) {
#pragma warning(suppress : 4996)
			save_slot_names[i].setColor(data->main_yellow);
		}
#pragma warning(suppress : 4996)
		save_slot_names[save_index].setColor(data->main_blue);
	}
	if (!thread_i) {
		handleInput(dt);
	}
	if (!thread_c) {
		collision();
	}
}

void MapMaker::handleInput(float dt)
{
	resetKeys();
	//	If not on save slot section then look for clicks that highlight the grid
	if (!removed_focus) {
		if (!area_mode) {
			if (is[0] && !was[0]) {
				//	Handle mouse input when selecting elements on the grid
				if (sf::Mouse::getPosition().x > 65.0f && sf::Mouse::getPosition().x < 1345.0f && sf::Mouse::getPosition().y > 65.0f && sf::Mouse::getPosition().y < 785.0f) {
					locked_x = current_x;
					locked_y = current_y;
					if (current_highlighted[0]) {
						if (current_active) {
							current_active->setActive(false);
							current_active = NULL;
						}

						current_active = current_highlighted[0];
						current_active->setActive(true);

						current_highlighted[0] = NULL;
					}
				}
				else if (index == -1) {
					if (current_active) {
						current_active->setActive(false);
						current_active = NULL;
					}
				}

				if (current_active) {
					switch (index) {
					case -1:
						//	Clicked on nothing of interest
						break;
					case 0:
						current_active->setElement(air);
						break;
					case 1:
						current_active->setElement(ground);
						break;
					case 2:
						current_active->setElement(enemy);
						break;

					case 3:
						if (current_spawn) {
							current_spawn->setElement(air);
							current_spawn = NULL;
						}
						current_active->setElement(spawn);
						current_spawn = current_active;
						break;

					case 4:
						if (current_win) {
							current_win->setElement(air);
							current_win = NULL;
						}
						current_active->setElement(spawn2);
						current_win = current_active;
						break;
					case 5:
						//	Pick save slot
						removed_focus = true;
						saved_prompt = false;
						break;
					}
				}
				checkSavePermission();
			}
		}
		else {
			if (is[0] && !was[0]) {
				if (current_active) {
					sf::Vector2f start_coords = current_active->grid_coords;
					sf::Vector2f end_coords = current_highlighted[0]->grid_coords;
					for (int i = start_coords.x; i < end_coords.x; i++) {
						for (int j = start_coords.y; j < end_coords.y; j++) {
							grid_[i][j].setHigh(true);
							grid_[i][j].update();
						}
					}
				}
			}
		}
	}
	else {		
		if (is[0] && !was[0]) {
			if (save_index != -1) {
				if (save_index == 3) {
					removed_focus = false;
					saved_prompt = false;
				}
				else if (saveLevel(save_index)) {
					saved_prompt = true;
				}				
			}
		}
	}
	if (is[1] && !was[1]) {
		data->active_focus = data->enum_pause;
	}
	if (data->input.isKeyDown(sf::Keyboard::M)) {
		exit(0);
	}
}

void MapMaker::collision()
{
	//	If not at save slot menu, then update collision for grid selection
	if (!removed_focus) {
		if (!area_mode) {
			for (int x = 0; x < 32; x++) {
				for (int y = 0; y < 18; y++) {
					if (sf::Mouse::getPosition().x < (grid_[x][y].border.getPosition().x + grid_[x][y].border.getSize().x) && sf::Mouse::getPosition().x >(grid_[x][y].border.getPosition().x)) {
						if (sf::Mouse::getPosition().y > (grid_[x][y].border.getPosition().y) && sf::Mouse::getPosition().y < (grid_[x][y].border.getPosition().y + grid_[x][y].border.getSize().y)) {
							grid_[x][y].setHigh(true);
							current_highlighted[0] = &grid_[x][y];
							current_x = x;
							current_y = y;
						}
						else {
							grid_[x][y].setHigh(false);
						}
					}
					else {
						grid_[x][y].setHigh(false);
					}
					grid_[x][y].update();
				}
			}
		}
		else {
			for (int x = 0; x < 32; x++) {
				for (int y = 0; y < 18; y++) {
					if (sf::Mouse::getPosition().x < (grid_[x][y].border.getPosition().x + grid_[x][y].border.getSize().x) && sf::Mouse::getPosition().x >(grid_[x][y].border.getPosition().x)) {
						if (sf::Mouse::getPosition().y > (grid_[x][y].border.getPosition().y) && sf::Mouse::getPosition().y < (grid_[x][y].border.getPosition().y + grid_[x][y].border.getSize().y)) {
							grid_[x][y].setHigh(true);
							current_highlighted[0] = &grid_[x][y];
							current_x = x;
							current_y = y;
						}
						else {
							grid_[x][y].setHigh(false);
						}
					}
					else {
						grid_[x][y].setHigh(false);
					}
					grid_[x][y].update();
				}
			}
		}
	}
	else {
		save_index = -1;
		for (int i = 0; i < 4; i++) {
			if (sf::Mouse::getPosition().x < save_slot_bg[i].getPosition().x + save_slot_bg[i].getSize().x && sf::Mouse::getPosition().x > save_slot_bg[i].getPosition().x) {
				if (sf::Mouse::getPosition().y < save_slot_bg[i].getPosition().y + save_slot_bg[i].getSize().y && sf::Mouse::getPosition().y > save_slot_bg[i].getPosition().y) {
					save_index = i;
				}
			}
		}
	}
}

void MapMaker::checkSavePermission()
{
	is_spawn_placed = false;
	is_win_placed = false;
	for (int x = 0; x < 32; x++) {
		for (int y = 0; y < 18; y++) {
			if (grid_[x][y].getElement() == spawn) {
				is_spawn_placed = true;
			}
			if (grid_[x][y].getElement() == spawn2) {
				is_win_placed = true;
			}
			if (is_spawn_placed && is_win_placed) {
				allow_save = true;
				break;
			}
			else {
				allow_save = false;
			}
		}
	}
}

bool MapMaker::saveLevel(int slot_)
{	
	std::ofstream level_file;
	switch (slot_) {
	case 0:
		//	Save in slot 1
		level_file.open("custom_level1.csv");
		break;
	case 1:
		//	Save in slot 2
		level_file.open("custom_level2.csv");
		break;
	case 2:
		//	Save in slot 3
		level_file.open("custom_level3.csv");
		break;
	}	
	//	Look through for all air places and get count
	int air_count = 0;
	for (int x = 0; x < 32; x++) {
		for (int y = 0; y < 18; y++) {
			if (grid_[x][y].getElement() == air) {
				air_count++;
			}
		}
	}		
	//	Firstly store the count
	level_file << air_count;
	level_file << ",";
	//	Then write to the file all the coordinates
	for (int x = 0; x < 32; x++) {
		for (int y = 0; y < 18; y++) {
			if (grid_[x][y].getElement() == air) {
				level_file << x;
				level_file << ",";
				level_file << y;
				level_file << ",";
			}
		}
	}
	//	Look for ground
	int ground_count = 0;
	for (int x = 0; x < 32; x++) {
		for (int y = 0; y < 18; y++) {
			if (grid_[x][y].getElement() == ground) {
				ground_count++;
			}
		}
	}	
	level_file << ground_count;
	level_file << ",";
	for (int x = 0; x < 32; x++) {
		for (int y = 0; y < 18; y++) {
			if (grid_[x][y].getElement() == ground) {
				level_file << x;
				level_file << ",";
				level_file << y;
				level_file << ",";				
			}
		}
	}
	//	Look for enemy spawns
	int spawn_count = 0;
	for (int x = 0; x < 32; x++) {
		for (int y = 0; y < 18; y++) {
			if (grid_[x][y].getElement() == enemy) {
				spawn_count++;
			}
		}
	}	
	level_file << spawn_count;
	level_file << ",";
	for (int x = 0; x < 32; x++) {
		for (int y = 0; y < 18; y++) {
			if (grid_[x][y].getElement() == enemy) {
				level_file << x;
				level_file << ",";
				level_file << y;
				level_file << ",";				
			}
		}
	}
	//	Look for spawn point
	int user_spawn = 0;
	for (int x = 0; x < 32; x++) {
		for (int y = 0; y < 18; y++) {
			if (grid_[x][y].getElement() == spawn) {
				level_file << x;
				level_file << ",";
				level_file << y;
				level_file << ",";
				user_spawn = 1;
				break;
			}
		}
	}	
	//	Look for win spot
	int win_spawn = 0;
	for (int x = 0; x < 32; x++) {
		for (int y = 0; y < 18; y++) {
			if (grid_[x][y].getElement() == spawn2) {
				level_file << x;
				level_file << ",";
				level_file << y;
				level_file << ",";
				win_spawn = 1;
				break;
			}
		}
	}
	level_file.close();
	//	Send all level information to the manager help in the global resources
	data->level_manager.saveLevel(air_count, ground_count, spawn_count, user_spawn, win_spawn, slot_);
	return true;
}

void MapMaker::resetKeys()
{
	for (int i = 0; i < 2; i++) {
		was[i] = is[i];
	}
	is[0] = sf::Mouse::isButtonPressed(sf::Mouse::Left);
	is[1] = data->input.isKeyDown(sf::Keyboard::Escape);
}

void MapMaker::render()
{
	window->clear(sf::Color::Black);

	window->draw(map_border);
	//	Draw grids
	for (int x = 0; x < 32; x++) {
		for (int y = 0; y < 18; y++) {
			window->draw(grid_[x][y].border);
		}
	}

	//	UI
	window->draw(grid_info);
	for (int i = 0; i < 6; i++) {
		window->draw(options_bg[i]);
		window->draw(options[i]);
	}
	if (show_hint) {
		window->draw(save_hint_bg);
		window->draw(save_hint);		
	}

	//	Save slot UI
	if (removed_focus) {
		window->draw(dim_bg);
		window->draw(save_bg);
		window->draw(save_bg_accent);

		window->draw(save_info);

		if (save_index != -1) {
			window->draw(save_slot_bg[save_index]);
		}
		for (int i = 0; i < 4; i++) {			
			window->draw(save_slot_names[i]);
		}
		if (saved_prompt) {
			window->draw(save_success_bg);
			window->draw(save_success_msg);
		}
	}

	if (data->active_focus != data->enum_pause) {
		window->display();
	}
	
}

void MapMaker::clean()
{

}


