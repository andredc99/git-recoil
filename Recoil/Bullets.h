#pragma once
#include "SpriteTemplate.h"
#include "Resources.h"

class Bullets :
	public SpriteTemplate
{
public:
	Bullets();
	~Bullets();

	void spawn(Resources* data_, b2Vec2 direction_, sf::Vector2f position_);
	void update();
	void kill();

	bool bullet_active;

private:
	const int scale = 30;

	b2Body* bullet_bodies;

	bool waiting_to_die;
};

