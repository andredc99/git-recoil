#include "Gun.h"



Gun::Gun()
{
}


Gun::~Gun()
{
}

bool Gun::init()
{
	bullets	= max_bullets;

	return true;
}

void Gun::reload()
{
	bullets = max_bullets;
}

void Gun::spawnBullet(Resources* data_, b2Vec2 direction_, sf::Vector2f position_)
{
	for (int i = 0; i < 2; i++) {
		if (!bullet_[i].bullet_active) {
			bullet_[i].spawn(data_, direction_, position_);
			break;
		}
	}
}

void Gun::updateBullets()
{
	for (int i = 0; i < 2; i++) {
		if (bullet_[i].bullet_active) {
			bullet_[i].update();
		}
	}
}