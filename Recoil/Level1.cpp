#include "Level1.h"

Level1::Level1()
{
}

bool Level1::init(Resources* data_, sf::RenderWindow* window_)
{
	data = data_;
	window = window_;
	window->setFramerateLimit(60);
	window->setMouseCursorVisible(true);
	data->last_focus = data->enum_level;
	//	Load vectors from the requested save slot and set file to none
	if (data->level_manager.getLoadedSlotNum() != -1) {
		data->level_manager.readLevel(data->level_manager.getLoadedSlotNum());
	}	
	else {
		data->level_manager.loadDefault();
	}

	//	Debug text declaration
	debug_text.setFont(data->default_font);
	debug_text.setCharacterSize(20);
	debug_text.setPosition(0, 0);

	b2Vec2 gravity(0.0f, 20.0f);
	data->world = new b2World(gravity);

	geo.init(data);
	//	Get player position from world editor and init player
	player1.init(data, true, data->level_manager.spawn_coords);
	player2.init(data, true, data->level_manager.spawn2_coords);
	geo.create_platform(data->level_manager.platform_coords);

	geo.create_border();

	//	Set screen size to size of window
	player_view.setSize(window->getSize().x, window->getSize().y);
	shake_view.setSize(window->getSize().x, window->getSize().y);

	//	Initialise background sprite
	background_sprite.setPosition(0.0f, 0.0f);
	background_sprite.setSize(sf::Vector2f(3840.0f, 1080.0f));
	background_sprite.setTexture(&data->bg_cloud);

	tree_sprites.setPosition(0.0f, 0.0f);
	tree_sprites.setSize(sf::Vector2f(1920.0f, 1080.0f));
	tree_sprites.setTexture(&data->bg_tree);

	vignette.setPosition(0.0f, 0.0f);
	vignette.setSize(sf::Vector2f(1920.0f, 1080.0f));
	sf::Color shadow;
	shadow.r = 255.0f;
	shadow.g = 255.0f;
	shadow.b = 255.0f;
	shadow.a = 96.0f;
#pragma warning(suppress : 4996)
	vignette.setFillColor(shadow);

	//	Screen shake focus
	shake_focus.setSize(sf::Vector2f(10.f, 10.f));
	shake_focus.setPosition(960, 1080 / 2);
	shaking = false;
	shake_time = 0.0f;
	shake_cooldown = 0.1f;

	//	How long will the tracer stay on screen
	tracer_cooldown = 0.8f;

	//	Set what player we are, currently always player 1
	data->setPlayer(false);
	if (!data->getPlayer()) {
		active_player = &player1;
		opponent = &player2;
	}
	else {
		active_player = &player2;
		opponent = &player1;
	}

	//	Create UI
	UI.initUI(data);

	mouse_released = false;
	debugging = false;

	return true;
}

void Level1::update(float dt, bool thread_input, bool thread_collision)
{
	if (!data->input.isMouseLeftDown()) {
		mouse_released = true;
	}
	//	Level update done in this if statement
	if (mouse_released)
	{
		data->world->Step(1 / 60.0f, 8, 3);		

		player_view.setCenter(active_player->getPosition());
		shake_view.setCenter(shake_focus.getPosition());
		active_player->update(dt);				

		//	Get local version of player raycast
		player_ray[0] = active_player->getLOS(0);
		player_ray[1] = sf::Vector2f(intersection_p.x, intersection_p.y);		

		//	Set player hasn't fired before checking if player has fired
		player_info_.shot_fired = false;
		player_info_.impact_point = b2Vec2(0, 0);
		//	Player has fired
		if (active_player->getTracerFlag()) {
			//	Fill network struct with firing information before acting
			player_info_.shot_fired = true;
			player_info_.impact_point = intersection_p;

			active_player->setTracerFlagSeen();
			displayTracer();			
			shake_focus.setPosition(955.f, shake_focus.getPosition().y);
			shaking = true;
		}
		//	Move background
		background_sprite.setPosition(background_sprite.getPosition().x - 0.1f, background_sprite.getPosition().y);
		if (background_sprite.getPosition().x <= -1920.f) {
			background_sprite.setPosition(0.0f, 0.0f);
		}
		//	Screen shake
		if (shaking) {
			shakeScreen(dt);
		}

		//	Update UI
		UI.update(dt, active_player->isReloading(), active_player->getReloadRatio(), active_player->gun_.bullets, active_player->getPosition(), active_player->getHealth());

		if (!thread_input) {
			handleInput(dt);
		}

		if (!thread_collision) {
			collision();
		}

		resetKeys();
		cooldowns(dt);

		//	Fill player info to send to opponent
		player_info_.body_position = active_player->getBody()->GetPosition();
		player_info_.health = active_player->getHealth();		
	}
}

void Level1::handleInput(float dt)
{
	if (is[0] && !was[0]) {
		data->active_focus = data->enum_pause;
	}
	if (is[1] && !was[1]) {
		debugging = !debugging;		
	}
}

void Level1::collision()
{
	// get the head of the contact list
	b2Contact* contact = data->world->GetContactList();
	// get contact count
	int contact_count = data->world->GetContactCount();

	for (int contact_num = 0; contact_num < contact_count; ++contact_num)
	{
		if (contact->IsTouching())
		{
			// get the colliding bodies
			b2Body* bodyA = contact->GetFixtureA()->GetBody();
			b2Body* bodyB = contact->GetFixtureB()->GetBody();

			// DO COLLISION RESPONSE HERE
			Player* player_ptr = NULL;
			Bullets* bullet_ptr = NULL;
			GroundPiece* geometry_ptr = NULL;
			RampPiece* ramp_piece_ptr = NULL;
			WinCondition* win_ptr = NULL;

			SpriteTemplate* gameObjectA = NULL;
			SpriteTemplate* gameObjectB = NULL;

			gameObjectA = (SpriteTemplate*)bodyA->GetUserData();
			gameObjectB = (SpriteTemplate*)bodyB->GetUserData();

			if (gameObjectA)
			{
				if (gameObjectA->getType() == PLAYER) {
					player_ptr = (Player*)bodyA->GetUserData();
				}
				if (gameObjectA->getType() == BULLET) {
					bullet_ptr = (Bullets*)bodyA->GetUserData();
				}
				if (gameObjectA->getType() == GEOMETRY) {
					geometry_ptr = (GroundPiece*)bodyA->GetUserData();
				}
				if (gameObjectA->getType() == RAMP) {
					ramp_piece_ptr = (RampPiece*)bodyA->GetUserData();
				}
				if (gameObjectA->getType() == WIN) {
					win_ptr = (WinCondition*)bodyA->GetUserData();
				}
			}

			if (gameObjectB)
			{
				if (gameObjectB->getType() == PLAYER) {
					player_ptr = (Player*)bodyB->GetUserData();
				}
				if (gameObjectB->getType() == BULLET) {
					bullet_ptr = (Bullets*)bodyB->GetUserData();
				}
				if (gameObjectB->getType() == GEOMETRY) {
					geometry_ptr = (GroundPiece*)bodyB->GetUserData();
				}
				if (gameObjectB->getType() == RAMP) {
					ramp_piece_ptr = (RampPiece*)bodyB->GetUserData();
				}
				if (gameObjectB->getType() == WIN) {
					win_ptr = (WinCondition*)bodyB->GetUserData();
				}
			}
		}
		// Get next contact point
		contact = contact->GetNext();
	}

	//	Player raycast collision
	b2RayCastInput ray_in;
	ray_in.p1 = active_player->getRayPointScaled(0);
	ray_in.p2 = active_player->getRayPointScaled(1);
	ray_in.maxFraction = 1.0f;

	//	Check every fixture of every body to find closest
	//	Fraction of the line where the intersection is taking place
	float intersection_fraction = 1.0f;
	b2Vec2 intersection_normal(0, 0);
	for (b2Body* b = data->world->GetBodyList(); b; b = b->GetNext()) {
		for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext()) {			
			b2RayCastOutput output;			
			if (!f->RayCast(&output, ray_in, 0)) {
				continue;
			}
			if (output.fraction < intersection_fraction) {
				intersection_fraction = output.fraction;
				intersection_normal = output.normal;
			}
		}
	}	

	intersection_p = active_player->getRayPoint(0) + intersection_fraction * (active_player->getRayPoint(1) - active_player->getRayPoint(0));	
}

void Level1::cooldowns(float dt)
{
	if (display_tracer) {
		tracer_current += dt;
		if (tracer_current >= tracer_cooldown) {
			tracer_current = 0.0f;
			display_tracer = false;
		}
	}
}

void Level1::resetKeys()
{
	for (int i = 0; i < 2; i++) {
		was[i] = is[i];
	}
	is[0] = data->input.isKeyDown(sf::Keyboard::Escape);
	is[1] = data->input.isKeyDown(sf::Keyboard::F1);
}

void Level1::displayTracer()
{
	tracer[0] = player_ray[0];
	tracer[1] = sf::Vector2f(intersection_p.x, intersection_p.y);		
	display_tracer = true;
}

void Level1::shakeScreen(float dt)
{	
	shake_time += dt;
	if (shake_time >= shake_cooldown) {
		shaking = false;
		shake_focus.setPosition(960.f, shake_focus.getPosition().y);
		shake_time = 0.f;
		return;
	}
	if (shake_focus.getPosition().x < (960.0f)) {
		shake_focus.setPosition(shake_focus.getPosition().x + 4.0f, shake_focus.getPosition().y);
		return;
	}
	if (shake_focus.getPosition().x > (960.0f)) {
		shake_focus.setPosition(shake_focus.getPosition().x - 4.0f, shake_focus.getPosition().y);
		return;
	}
}

void Level1::render()
{
	window->clear(sf::Color::Black);

	if (shaking) {
		window->setView(shake_view);
	}
	else {
		window->setView(window->getDefaultView());
	}

	//	Draw background first
	window->draw(background_sprite);
	window->draw(tree_sprites);
	window->draw(vignette);

	window->draw(player1);
	window->draw(player2);

	for (int i = 0; i < geo.getActivePieceCount(); i++) {		
		window->draw(geo.ground_pieces[i].sprite);		
	}

	window->draw(active_player->getCursor());

	if (display_tracer) {
		window->draw(tracer, 2, sf::Lines);
	}	

	//	Render UI
	UI.render(window);

	//	Debugging information
	if (debugging) {
		window->draw(player_ray, 2, sf::Lines);
		window->draw(debug_text);
		window->draw(shake_focus);
	}

	if (data->active_focus != data->enum_pause) {
		window->display();
	}
}

void Level1::initUI()
{

}

void Level1::clean()
{	
	geo.cleanWorld();
	geo.resetActivePieceCount();

	data = NULL;
	delete data;

	window = NULL;
	delete window;
}