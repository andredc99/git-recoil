#pragma once
#include "Resources.h"
#include "SpriteTemplate.h"

class GroundPiece : public SpriteTemplate
{
public:
	GroundPiece();
};

class RampPiece : public SpriteTemplate
{
public:
	RampPiece();
	sf::ConvexShape shape_;
};

class WorldBuilder
{
public:
	WorldBuilder();

	struct world_item {
		GroundPiece sprite;
		RampPiece convex_sprite;
		b2Body* body;
	};

	bool init(Resources* data_);
	void create_platform(std::vector<b2Vec2> coords);
	void create_ramp(std::vector<b2Vec2> coords);
	void create_border();

	b2Body* BodyInformation(int i_);

	void cleanWorld();

	world_item	ground_pieces[32*18];	
	b2Body*		border_pieces[4]; // 0 = top; 1 = bottom; 2 = right; 3 = left;

	void resetActivePieceCount() { active_piece_index = 0; active_ramp_index = 0; }
	int getActivePieceCount()	 { return active_piece_index; }
	int getActiveRampCount()	 { return active_ramp_index; }

private:
	Resources* data;	

	int active_piece_index;
	int active_ramp_index;
};

