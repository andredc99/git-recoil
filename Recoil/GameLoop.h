#pragma once
#include "LevelMaster.h"

class GameLoop
{
public:
	GameLoop();
	~GameLoop();

	void run(LevelMaster *level_, int thread_count_, float dt);
	void mainLoop();
};

