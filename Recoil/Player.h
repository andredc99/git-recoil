#pragma once
#include "SpriteTemplate.h"
#include "Resources.h"

#include "Cursor.h"
#include "Gun.h"

class Player :
	public SpriteTemplate
{
public:
	Player();
	~Player();

	bool init(Resources* data_, bool has_gun, b2Vec2 position_);
	void update(float dt);
	void handleInput(float dt, b2Vec2 direction_);

	//	Gun and jump setters
	inline void changeGunPermisions(bool b)		{ can_shoot = b; }
	inline void changeJumpPermisions(bool b)	{ can_jump = b; }
	//	Jump getter
	inline bool getJumpPermission()	{ return can_jump; }	

	//	Health getter and setter
	inline void setHealth(int i_)	{ health = i_; }
	inline int  getHealth()			{ return health; }

	//	Get los ray and points
	inline sf::Vertex	getLOS(int index_)				{ return raycast[index_]; }
	inline b2Vec2		getRayPoint(int index_)			{ return p_[index_]; }
	inline b2Vec2		getRayPointScaled(int index_)	{ return s_[index_]; }

	//	Tracer getter and setter	
	inline sf::Vertex	getTracer(int index_)			{ return tracer[index_]; }
	inline bool			getTracerFlag()					{ return new_tracer; }
	inline void			setTracerFlagSeen()				{ new_tracer = false; }

	//	Move cursor when using player view
	void nudgeCursor(sf::Vector2f move_) { cursor_.nudge(move_); }

	//	Reload getter
	bool isReloading() { return reloading; }
	float getReloadRatio() { return reload_cooldown / reload_time; }

	sf::RectangleShape getCursor() { return cursor_.cursor_sprite; }
	b2Body* getBody() { return linked_body; }
	Gun gun_;

private:
	Resources* data;
	b2Body* linked_body;
	Cursor cursor_;

	void resetKeys();

	//	LOS
	sf::Vertex raycast[2];
	b2Vec2 p_[2];	
	b2Vec2 s_[2];
	
	sf::Vertex tracer[2];
	bool new_tracer;

	//	Key trackers
	bool was[3];
	bool is[3];

	//	Player permissions
	bool can_shoot;
	bool can_jump;	

	//	Reload rules
	bool reloading;
	float reload_time;
	float reload_cooldown;

	int health;
};

