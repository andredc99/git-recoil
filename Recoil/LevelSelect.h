#pragma once
#include "LevelMaster.h"
class LevelSelect
{
public:
	LevelSelect();

	bool init(Resources* data_, sf::RenderWindow* window_);
	void handleInput(float dt);
	void render();
	void clean();

private:

	void update();
	void resetKeys();
	void activate(int index_);

	Resources* data;
	sf::RenderWindow* window;	

	//	Prevent quick swapping
	bool mouse_released;
	bool enter_released;

	//	Key refreshers
	bool is[4];		// 0 = mouse left click; 1 = arrow down; 2 = arrow up; 3 = enter;
	bool was[4];	// 0 = mouse left click; 1 = arrow down; 2 = arrow up; 3 = enter;

	//	UI
	sf::RectangleShape background;

	//	Level Picker
	sf::Text title;

	sf::Text			selections[5];
	sf::RectangleShape	highlights[5];	
	float				targets[5];
	sf::RectangleShape	highlight;
	float				highlight_y;
	int					selection_index;	
};

