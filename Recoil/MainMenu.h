#pragma once
#include "Resources.h"

class MainMenu
{
public:
	MainMenu();

	bool init(Resources* data_, sf::RenderWindow* window_);
	void handleInput(float dt);
	void render();
	void clean();

private:
	Resources* data;
	sf::RenderWindow* window;
	
	void resetKeys();
	void update();
	void activate(int index_);

	//	Prevent quick swapping
	bool mouse_released;
	bool enter_released;

	//	Key refreshers
	bool is[4];		// 0 = mouse left click; 1 = arrow down; 2 = arrow up; 3 = enter;
	bool was[4];	// 0 = mouse left click; 1 = arrow down; 2 = arrow up; 3 = enter;

	//	Aesthetic
	sf::RectangleShape background;

	//	Menu selections
	sf::Text			selections[4];		// 0 = play; 1 = world editor; 2 = options; 3 = exit;
	sf::RectangleShape	highlights[4];		// 0 = play; 1 = world editor; 2 = options; 3 = exit; COLLISION DETECTION ONLY
	float				targets[4];			// 0 = play; 1 = world editor; 2 = options; 3 = exit;
	sf::RectangleShape	highlight;
	float				highlight_y;
	int					selection_index;	// 0 = play; 1 = world editor; 2 = options; 3 = exit;
	sf::Text			title;
	sf::Sprite			revolver;
	
	sf::Font title_font;
};

