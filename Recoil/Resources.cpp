#include "Resources.h"

Resources::Resources()
{
	resolution[0] = sf::Vector2f(1920, 1080);
	resolution[1] = sf::Vector2f(1600, 900);
	resolution[2] = sf::Vector2f(1280, 720);
	resolution[3] = sf::Vector2f(1024, 576);

	current_resolution = resolution[0];

	//	Define blue colour
	main_blue.r = 59;
	main_blue.g = 79;
	main_blue.b = 255;

	//	Define yellow colour
	main_yellow.r = 255;
	main_yellow.g = 235;
	main_yellow.b = 59;

	//	Define ui when the element is empty	
	ui_full.r = 17.0f;
	ui_full.g = 17.0f;
	ui_full.b = 17.0f;

	//	Define ui colour when the element is full
	ui_empty = ui_full;
	ui_empty.a = 96.0f;

	//	Define default font
	if (!default_font.loadFromFile("Fonts/beb.ttf")) {
		std::cout << "error loading main font" << std::endl;
	}
	//	Define default font
	if (!game_font.loadFromFile("Fonts/pixel.ttf")) {
		std::cout << "error loading pixel font" << std::endl;
	}

	//	Get available threads
	cpu_thread_count = std::thread::hardware_concurrency();

	//	Set active focus to menu
	active_focus = enum_menu;

	//	Load textures
	ground_tex.loadFromFile("Assets/Level/tile_ground.png");
	bg_cloud.loadFromFile("Assets/Level/bg_clouds.png");
	bg_tree.loadFromFile("Assets/Level/bg_trees.png");

	//	UI
	icon_reload.loadFromFile("Assets/Icons/key_r.png");
}