#include "LoadingScreen.h"



LoadingScreen::LoadingScreen()
{
}


bool LoadingScreen::init(Resources* data_, sf::RenderWindow* window_)
{
	//	Get values
	data = data_;
	window = window_;
	window->setFramerateLimit(60);

	//	Declare loading text
	loading_.setFont(data->default_font);
	loading_.setCharacterSize(60);
	loading_.setPosition((window->getSize().x / 2) - 60, (window->getSize().y / 2) - 50);
#pragma warning(suppress : 4996) 
	loading_.setColor(data->main_blue);
	loading_.setString("LOADING...");

	//	Declare percentage int
	percentage.setFont(data->default_font);
	percentage.setCharacterSize(60);
	percentage.setOrigin(loading_.getCharacterSize() / 2, loading_.getCharacterSize() / 2);
	percentage.setPosition((window->getSize().x / 2) + 10, (window->getSize().y / 2) + 40);
#pragma warning(suppress : 4996) 
	percentage.setColor(data->main_blue);
	percentage.setString(std::to_string(0) + "%");

	bg.setFillColor(data->main_yellow);
	bg.setPosition(0, 0);
	sf::Vector2f size_vec(window->getSize().x, window->getSize().y);
	bg.setSize(size_vec);

	return true;
}

void LoadingScreen::render(int p)
{
	window->clear(sf::Color::Black);

	window->draw(bg);
	window->draw(loading_);
	if (p != -1) {
		percentage.setString(std::to_string(p) + "%");
	}
	else {
		percentage.setString("...");
	}
	window->draw(percentage);

	window->display();
}

void LoadingScreen::clean()
{
	//	Clean loading screen if needing used again
	data = NULL;
	delete data;

	window = NULL;
	delete window;
}