#include "UImanager.h"

UImanager::UImanager()
{

}

void UImanager::initUI(Resources* data_)
{
	data = data_;	

	//	Ammo counter
	ammo.setPosition(1835, 965);
	ammo.setFont(data->default_font);
#pragma warning(suppress : 4996)
	ammo.setColor(data->main_blue);
	ammo.setCharacterSize(70);

	ammo_misc.setPosition(1880, 970);
	ammo_misc.setSize(sf::Vector2f(5.0f, 80.0f));
#pragma warning(suppress : 4996)
	ammo_misc.setFillColor(data->main_blue);

	//	Reload hint
	reload_icon.setPosition(30.0f, 30.0f);
	reload_icon.setSize(sf::Vector2f(60.0f, 60.0f));
	reload_icon.setTexture(&data->icon_reload);

	reload_hint.setFont(data->game_font);
	reload_hint.setCharacterSize(30);
	reload_hint.setPosition(105.0f, 40.0f);
	reload_hint.setString("Reload");
	sf::Color ui_light;
	ui_light.r = 89.0f;
	ui_light.g = 89.0f;
	ui_light.b = 89.0f;
#pragma warning(suppress : 4996)
	reload_hint.setColor(ui_light);
	reload_hint.setOutlineThickness(2);
#pragma warning(suppress : 4996)
	reload_hint.setOutlineColor(data->ui_full);

	reload_tracker.setPosition(103.0f, 81.0f);
	reload_tracker.setSize(sf::Vector2f(132.0f, 4.0f));
#pragma warning(suppress : 4996)
	reload_tracker.setFillColor(data->ui_full);

	reload_tracker_shadow.setPosition(103.0f, 81.0f);
	reload_tracker_shadow.setSize(sf::Vector2f(132.0f, 4.0f));
#pragma warning(suppress : 4996)
	reload_tracker_shadow.setFillColor(data->ui_empty);

	//	Health Indicator		
	health_indicator[0].setSize(sf::Vector2f(28.0f, 4.0f));
#pragma warning(suppress : 4996)
	health_indicator[0].setFillColor(data->ui_full);
	health_indicator[1].setSize(sf::Vector2f(28.0f, 4.0f));
#pragma warning(suppress : 4996)
	health_indicator[1].setFillColor(data->ui_full);
}

void UImanager::update(float dt, bool is_reloading_, float reload_ratio_, 
	int bullet_count_, sf::Vector2f player_position_, int player_health_)
{
	//	Update ammo count
	if (is_reloading_) {
		reload_tracker.setSize(sf::Vector2f(132.0f * reload_ratio_, 4.0f));
	}
	else {
		switch (bullet_count_) {
		case 0:
			reload_tracker.setSize(sf::Vector2f(132.0f * 0.0f, 4.0f));
			break;
		case 1:
			reload_tracker.setSize(sf::Vector2f(132.0f * 0.5f, 4.0f));
			break;
		case 2:
			reload_tracker.setSize(sf::Vector2f(132.0f * 1.0f, 4.0f));
			break;
		}
	}
	ammo.setString(std::to_string(bullet_count_));

	//	Update health
	health_indicator[0].setPosition(player_position_.x - 30, player_position_.y + 33);
	health_indicator[1].setPosition(player_position_.x + 2, player_position_.y + 33);
	switch (player_health_) {
	case 0:
#pragma warning(suppress : 4996)
		health_indicator[0].setFillColor(data->ui_empty);
#pragma warning(suppress : 4996)
		health_indicator[1].setFillColor(data->ui_empty);
		break;
	case 1:
#pragma warning(suppress : 4996)
		health_indicator[0].setFillColor(data->ui_full);
#pragma warning(suppress : 4996)
		health_indicator[1].setFillColor(data->ui_empty);
		break;
	case 2:
#pragma warning(suppress : 4996)
		health_indicator[0].setFillColor(data->ui_full);
#pragma warning(suppress : 4996)
		health_indicator[1].setFillColor(data->ui_full);
		break;
	}
}

void UImanager::render(sf::RenderWindow* window_)
{
	window_->draw(ammo);
	window_->draw(ammo_misc);

	window_->draw(reload_icon);
	window_->draw(reload_hint);
	window_->draw(reload_tracker_shadow);
	window_->draw(reload_tracker);

	for (int i = 0; i < 2; i++) {
		window_->draw(health_indicator[i]);
	}
}