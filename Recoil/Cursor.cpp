#include "Cursor.h"



Cursor::Cursor()
{
}


Cursor::~Cursor()
{
}

bool Cursor::init(Resources* data_)
{
	data = data_;

	sf::Vector2f size(10.0f, 10.0f);
	cursor_sprite.setSize(size);
	cursor_sprite.setOrigin(size.x / 2, size.y / 2);
	cursor_sprite.setFillColor(sf::Color::Green);

	return true;
}

void Cursor::update()
{
	cursor_sprite.setPosition(sf::Mouse::getPosition().x, sf::Mouse::getPosition().y);
}

void Cursor::nudge(sf::Vector2f move_)
{
	cursor_sprite.setPosition(cursor_sprite.getPosition().x + move_.x, cursor_sprite.getPosition().y + move_.y);
}