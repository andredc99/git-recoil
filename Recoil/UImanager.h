#pragma once
#include "Resources.h"

class UImanager
{
public:

	UImanager();
	void initUI(Resources* data_);
	void update(float dt, bool is_reloading_, float reload_ratio_, 
		int bullet_count_, sf::Vector2f player_position_, int player_health_);
	void render(sf::RenderWindow* window_);

private:
	Resources* data;	

	//sf::RectangleShape elements[32];

	//	UI elements
	sf::RectangleShape reload_icon;
	sf::RectangleShape reload_tracker;
	sf::RectangleShape reload_tracker_shadow;
	sf::Text reload_hint;

	sf::Text			ammo;
	sf::RectangleShape	ammo_misc;

	sf::RectangleShape health_indicator[2];
};

