#pragma once
#include "Resources.h"
#include "SpriteTemplate.h"
#include "Bullets.h"

class Gun
{
public:
	Gun();
	~Gun();

	bool init();
	void reload();
	void spawnBullet(Resources* data_, b2Vec2 direction_, sf::Vector2f position_);
	void updateBullets();

	int bullets;
	Bullets bullet_[2];

private:
	const int max_bullets = 2;
	const int scale = 30;


};

