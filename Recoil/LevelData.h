#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <string>
#include <algorithm>

#include <Box2D/Box2D.h>

class LevelData
{
public:
	LevelData();
	~LevelData();

	void saveLevel(int a, int g, int e, int s, int w, int slot_);
	void readLevel(int slot_);
	void loadDefault();

	//	Coordinates of placements inside level
	std::vector<b2Vec2> platform_coords;
	std::vector<b2Vec2> enemy_coords;
	b2Vec2				spawn_coords;
	b2Vec2				spawn2_coords;

	//	Getter for currently loaded
	inline void setLoadedSlotNum(int load_num_) { current_loaded = load_num_; }
	inline int getLoadedSlotNum()				{ return current_loaded; }

private:
	//	Count of each element type
	int element_defualt_data[5];	//	0 = air; 1 = ground; 2 = enemy; 3 = spawn; 4 = win;
	int element_data_count[3][5];	//	First element for slot

	int current_loaded;	//	Slot number; 0 means nothing currently loaded;
};

