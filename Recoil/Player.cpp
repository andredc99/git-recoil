#include "Player.h"

Player::Player()
{
}

Player::~Player()
{
}

bool Player::init(Resources* data_, bool has_gun, b2Vec2 position_)
{
	data = data_;
	sf::Vector2f size_(data->getResolution().x / 32, data->getResolution().y / 18);
	sf::Vector2f start_point(size_.x / 2, size_.y / 2);
	sf::Vector2f new_coords(start_point.x + (position_.x * size_.x), start_point.y + (position_.y * size_.y));
	linked_body = data->geo_mgr.object(data->world, new_coords, sf::Vector2f(60, 60), false);	
	setType(PLAYER);

	sf::Vector2f size(60.0f, 60.0f);
	setSize(size);
	setOrigin(size.x / 2, size.y / 2);
	setPosition(linked_body->GetPosition().x * data->scale, linked_body->GetPosition().y * data->scale);
	setRotation(linked_body->GetAngle() * 180 / b2_pi);
	setFillColor(sf::Color::Red);
	linked_body->SetLinearDamping(1.0f);
	linked_body->SetAngularDamping(100.0f);
	linked_body->SetUserData(this);

	health = 2;

	cursor_.init(data);
	gun_.init();
	can_shoot = has_gun;
	can_jump = true;	

	reloading = false;
	reload_time = 1.5f;
	reload_cooldown = 0.0f;

	return true;
}

void Player::update(float dt)
{
	//	Reset keys
	for (int i = 0; i < 3; i++) {
		was[i] = is[i];
	}
	is[0] = data->input.isKeyDown(sf::Keyboard::Space);
	is[1] = data->input.isMouseLeftDown();
	is[2] = data->input.isKeyDown(sf::Keyboard::R);

	setPosition(linked_body->GetPosition().x * data->scale, linked_body->GetPosition().y * data->scale);
	setRotation(linked_body->GetAngle() * 180 / b2_pi);

	//	Get mouse cursor angle
	b2Vec2 recoil_direction(cursor_.cursor_sprite.getPosition().x - getPosition().x, cursor_.cursor_sprite.getPosition().y - getPosition().y);
	recoil_direction.Normalize();

	//	Update jump permission
	if (linked_body->GetLinearVelocity().y == 0.0f) {
		can_jump = true;
	}
	else {
		can_jump = false;
	}

	//	Create a raycast from the player centre to the mouse cursor direction
	float ray_length = 1920.0f;
	p_[0] = b2Vec2(getPosition().x , getPosition().y);
	p_[1] = b2Vec2(p_[0] + ray_length * recoil_direction);
	//	Scaled down versions of the points to match the box2d representation
	s_[0] = b2Vec2(p_[0].x / 30, p_[0].y / 30);
	s_[1] = b2Vec2(p_[1].x / 30, p_[1].y / 30);
	//	Unscaled points displayed in the line format for the screen
	raycast[0] = sf::Vertex(sf::Vector2f(p_[0].x, p_[0].y));
	raycast[1] = sf::Vertex(sf::Vector2f(p_[1].x, p_[1].y));	
	
	//	Reload cooldown
	if (reloading) {
		can_shoot = false;
		reload_cooldown += dt;
		if (reload_cooldown >= reload_time) {
			reloading = false;
			can_shoot = true;
			reload_cooldown = 0.0f;
			gun_.reload();
		}
	}

	//	Update player children
	cursor_.update();
	gun_.updateBullets();
	handleInput(dt, recoil_direction);
}

void Player::handleInput(float dt, b2Vec2 direction_)
{
	b2Vec2 speed(90.0f, 0.0f);
	b2Vec2 jump(0.0f, 55.0f);
	float firepower = 115.0f;
	if (data->input.isKeyDown(sf::Keyboard::D) || data->input.isKeyDown(sf::Keyboard::Right)) {
		linked_body->ApplyForceToCenter(speed, true);
	}
	if (data->input.isKeyDown(sf::Keyboard::A) || data->input.isKeyDown(sf::Keyboard::Left)) {
		linked_body->ApplyForceToCenter(-speed, true);
	}
	if (is[0] && !was[0] && can_jump) {
		linked_body->ApplyLinearImpulseToCenter(-jump, true);
		can_jump = false;
	}
	if (is[1] && !was[1] && gun_.bullets > 0 && can_shoot) {		
		gun_.bullets--;

		direction_ *= -firepower;
		linked_body->ApplyLinearImpulseToCenter(direction_, true);
		tracer[0] = raycast[0];
		tracer[1] = raycast[1];
		new_tracer = true;
	}
	if (is[2] && !was[2] && gun_.bullets < 2 && !reloading) {
		reloading = true;		
	}
}