#pragma once
#include "Resources.h"

class Cursor
{
public:
	Cursor();
	~Cursor();

	bool init(Resources* data_);
	void update();
	void nudge(sf::Vector2f move_);

	sf::RectangleShape cursor_sprite;

private:
	Resources* data;
};

