#pragma once
#include "Resources.h"

class LoadingScreen
{
public:
	LoadingScreen();

	bool init(Resources* data_, sf::RenderWindow* window_);
	void render(int p);
	void clean();

private:
	Resources* data;
	sf::RenderWindow* window;

	//	Text and background colour assets
	sf::Text loading_;
	sf::Text percentage;
	sf::RectangleShape bg;
};

