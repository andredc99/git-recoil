#pragma once
#include <iostream>
#include <fstream>

#include <vector>
#include <iterator>
#include <string>
#include <algorithm>
#include <cstdio>

#include "LevelMaster.h"
#include "Grid.h"
#include "LevelData.h"

class MapMaker :
	public LevelMaster
{
public:
	MapMaker();
	
	bool init(Resources* data_, sf::RenderWindow* window_);
	void update(float dt, bool thread_input, bool thread_collsion);
	void handleInput(float dt);
	void collision();
	void render();
	void clean();

private:
	Resources* data;
	sf::RenderWindow* window;
	LevelData* level_mgr;
	
	void resetKeys();
	bool saveLevel(int slot_);
	void checkSavePermission();

	sf::RectangleShape map_border;

	//	Grid selections
	Grid	grid_[32][18];
	Grid*	current_active;
	Grid*	current_highlighted[32 * 18];
	Grid*	current_win;
	Grid*	current_spawn;

	bool area_mode;

	int		current_x;
	int		current_y;
	int		locked_x;
	int		locked_y;

	//	UI information
	sf::Text grid_info;

	sf::Text			options[6];		// 0 = air; 1 = ground; 2 = enemy; 3 = spawn; 4 = win; 5 = save;
	sf::RectangleShape	options_bg[6];	// 0 = air; 1 = ground; 2 = enemy; 3 = spawn; 4 = win; 5 = save;

	int index;							// 0 = air; 1 = ground; 2 = enemy; 3 = spawn; 4 = win;

	//	Save slot UI	
	sf::RectangleShape save_bg;
	sf::RectangleShape save_bg_accent;
	sf::RectangleShape dim_bg;

	sf::Text save_info;

	sf::Text			save_slot_names[4];
	sf::RectangleShape	save_slot_bg[4];
	int save_index;

	sf::Text			save_success_msg;
	sf::RectangleShape	save_success_bg;
	bool saved_prompt;

	//	Mouse selection refresher
	bool is[2];		//	0 = mouse 1; 1 = esc;
	bool was[2];	//	0 = mouse 1; 1 = esc;

	//	For loop escape
	bool break_b;

	//	Permissions for saving level
	bool is_spawn_placed;
	bool is_win_placed;
	bool allow_save;

	//	Save hint
	sf::Text			save_hint;
	sf::RectangleShape	save_hint_bg;
	bool show_hint;

	//	Save slot selection
	bool removed_focus;
};


