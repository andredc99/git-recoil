#pragma once
#include <iostream>
#include "Resources.h"
#include "Geometry.h"

#include "Player.h"
#include "Gun.h"
#include "WorldBuilder.h"

class Playground
{
public:
	Playground();	
	bool init(Resources* data_, sf::RenderWindow* window_);	
	void update(float dt);
	void handleInput(float dt);
	void collision();
	void render();
	void clean();

private:
	Resources* data;
	sf::RenderWindow* window;
	const float scale = 30.0f;

	//	Create player
	Player player;

	//	Create ground
	WorldBuilder world_builder;

	//	Line connecting cursor and player
	sf::Vertex line[2];

	//	Camera
	sf::View player_view;
};

